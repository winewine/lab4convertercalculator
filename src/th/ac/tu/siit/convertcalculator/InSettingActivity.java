package th.ac.tu.siit.convertcalculator;

import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class InSettingActivity extends Activity  implements OnClickListener {

	float interestRate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_in_setting);
		
		Button b1 = (Button)findViewById(R.id.btnFinish);
		b1.setOnClickListener(this);
		
		Intent i = this.getIntent();
		interestRate = i.getFloatExtra("interestRate", 0.1f);
		
		EditText etRate = (EditText)findViewById(R.id.etRate);
		etRate.setText(String.format(Locale.getDefault(), "%.2f", interestRate));
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.setting, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		EditText etRate = (EditText)findViewById(R.id.etRate);
		try {
			float r = Float.parseFloat(etRate.getText().toString());
			// Create on intent to wrap the return value
			Intent data = new Intent();
			// Add the return value to the intent
			data.putExtra("interestRate", r);
			// Set the intent as the result when this activity ends 
			this.setResult(RESULT_OK, data);
			// End this activity
			this.finish();
		} catch(NumberFormatException e) {
			Toast t = Toast.makeText(getApplicationContext(), 
					"Invalid exchange rate", Toast.LENGTH_SHORT);
			t.show();
			etRate.setText("");
			etRate.requestFocus();
		} catch(NullPointerException e) {
			Toast t = Toast.makeText(getApplicationContext(), 
					"Invalid exchange rate", Toast.LENGTH_SHORT);
			t.show();
			etRate.setText("");
			etRate.requestFocus();
		}
	}

}
