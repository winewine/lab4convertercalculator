package th.ac.tu.siit.convertcalculator;

import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class InterestActivity extends Activity implements OnClickListener {

	float interestRate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_interest);
		
		Button b1 = (Button)findViewById(R.id.btnConvertMoney);
		b1.setOnClickListener(this);
		
		Button b2 = (Button)findViewById(R.id.btnSetting);
		b2.setOnClickListener(this);
		
		TextView tvRate = (TextView)findViewById(R.id.tvRate);
		interestRate = Float.parseFloat(tvRate.getText().toString());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.exchange, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		
		if (id == R.id.btnConvertMoney) {
			EditText etDp = (EditText)findViewById(R.id.etDp);
			TextView tvTHB = (TextView)findViewById(R.id.tvTHB);
			EditText etYr = (EditText)findViewById(R.id.etYr);
			
			String res = "";
			try {
				float Dp = Float.parseFloat(etDp.getText().toString());
				float Yr = Float.parseFloat(etYr.getText().toString());
				float thb = (float) (Dp * Math.pow((1 + interestRate/100),Yr));
				res = String.format(Locale.getDefault(), "%.2f", thb);
			} catch(NumberFormatException e) {
				res = "Invalid input";
			} catch(NullPointerException e) {
				res = "Invalid input";
			}
			tvTHB.setText(res);
		}
		else if (id == R.id.btnSetting) {
			Intent i = new Intent(this, InSettingActivity.class);
			i.putExtra("interestRate", interestRate);
			startActivityForResult(i, 8888);
			// 9999  = a request code, it is a unique integer value for internally identifying
			//then return value
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 8888 && resultCode == RESULT_OK) {
			interestRate = data.getFloatExtra("interestRate", 0.1f);
			TextView tvRate = (TextView)findViewById(R.id.tvRate);
			tvRate.setText(String.format(Locale.getDefault(), "%.2f", interestRate));
		}
	}

	
}
